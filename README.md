# Heatmap

## Description

The repository contains a script to generate a heatmap from address (postal code, city, country) data.

## License

The repository is licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE](./LICENSE).
