# Participants map
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import io
import json
import folium
from folium.plugins import HeatMap
from geopy.geocoders import Nominatim
from PIL import Image

def get_location(address: dict[str,str]) -> list[str,str]:
    geolocator = Nominatim(user_agent="Mathezirkel")
    postal_code = (address["postalCode"] or "").strip()
    city = (address["city"] or "").strip()
    country = (address["country"] or "").strip()
    address_string = f"{postal_code} {city} {country}".strip()
    # If the address is not known, use place instead
    print(address_string)
    if address_string and (location := geolocator.geocode(address_string)):
        loc = [location.latitude, location.longitude]
        print(loc)
        return loc
    return [None, None]

def main():
    with open("heatmap.json", "r", encoding="utf-8") as file:
        data: list[dict[str,str]] = json.load(file)

    # Create a map centered at Violau
    heatmap = folium.Map(
        location=[48.45183927467093, 10.573874169313934],
        tiles="OpenStreetMap",
        zoom_start=5
    )
    folium.Marker(
        location=[48.45183927467093, 10.573874169313934],
        icon=folium.Icon(
            color='red', icon_color="white", icon="infinity", prefix="fa"
        )
    ).add_to(heatmap)

    points = [loc for address in data if (loc := get_location(address)) and loc[0] and loc[1]]

    HeatMap(points).add_to(heatmap)
    heatmap.save("output/heatmap.html")
    img_data = heatmap._to_png()
    img = Image.open(io.BytesIO(img_data))
    img.save("output/heatmap.png")

if __name__ == "__main__":
    main()
